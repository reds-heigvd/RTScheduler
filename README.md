## rtscheduler

The aim of this software is to show various schedulings for a set of tasks.
It allows to create periodic tasks and observe the scheduling with 4 algorithms:
- RM
- DM
- EDF
- LLF

It also exports Latex code that can be used in any document, with a specific package scheduling.sty.

To compile it, you need Qt ( https://www.qt.io/download ).
Go into the dev folder, and run:

```
qmake
make
```

The code is supplied "as is", with no guaranties. If you are interested in developing new features,
please do, and contact Yann Thoma : http://reds.heig-vd.ch/en/team/details/yann.thoma

