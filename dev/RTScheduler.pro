#-------------------------------------------------
#
# Project created by QtCreator 2014-11-10T11:23:31
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RTScheduler
TEMPLATE = app
CONFIG += debug
CONFIG += c++11

#Separate the build directories (no more on the different plateforms, prefer shadow build)
MOC_DIR = build/moc
OBJECTS_DIR = build/obj
UI_DIR = build/ui
RCC_DIR = build/rcc

DESTDIR = dist

INCLUDEPATH += src src/gui src/model src/processing src/schedulers

HEADERS += \
    src/appsettings.h \
    src/connect.h \
    src/model/rtproject.h \
    src/model/rtscheduler.h \
    src/model/rtschedulerfactory.h \
    src/model/rttask.h \
    src/model/rttaskset.h \
    src/processing/latexexporter.h \
    src/processing/rtprojectfilemanager.h \
    src/gui/arrowitem.h \
    src/gui/mainwindow.h \
    src/gui/schedulingscene.h \
    src/gui/taskswidget.h \
    src/model/command.h \
    src/gui/schedulingview.h \
    src/model/rtscheduling.h \
    src/schedulers/rmscheduler.h \
    src/schedulers/dmscheduler.h \
    src/schedulers/edfscheduler.h \
    src/schedulers/llfscheduler.h \
    src/schedulers/fifoscheduler.h \
    src/schedulers/priorityscheduler.h \
    src/schedulers/schedulinganalyzer.h

SOURCES += \
    src/appsettings.cpp \
    src/main.cpp \
    src/model/rtproject.cpp \
    src/model/rtscheduler.cpp \
    src/model/rtschedulerfactory.cpp \
    src/model/rttask.cpp \
    src/model/rttaskset.cpp \
    src/processing/latexexporter.cpp \
    src/processing/rtprojectfilemanager.cpp \
    src/gui/arrowitem.cpp \
    src/gui/mainwindow.cpp \
    src/gui/schedulingscene.cpp \
    src/gui/taskswidget.cpp \
    src/model/command.cpp \
    src/gui/schedulingview.cpp \
    src/model/rtscheduling.cpp \
    src/schedulers/rmscheduler.cpp \
    src/schedulers/dmscheduler.cpp \
    src/schedulers/edfscheduler.cpp \
    src/schedulers/llfscheduler.cpp \
    src/schedulers/fifoscheduler.cpp \
    src/schedulers/priorityscheduler.cpp \
    src/schedulers/schedulinganalyzer.cpp

RESOURCES += \
    rtscheduler.qrc

