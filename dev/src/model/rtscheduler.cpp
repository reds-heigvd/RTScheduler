/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "rtscheduler.h"

Slot::Slot(WHAT what, RtTask *activeTask) :
    what(what), activeTask(activeTask)
{

}

RtScheduler::RtScheduler(SchedulingAnalyzer *analyzer) :
    analyzer(analyzer)
{
    analysisPeriod = 0;
}

RtScheduler::~RtScheduler()
{
    clearScheduling();
}

void RtScheduler::setSchedulingAnalyzer(SchedulingAnalyzer *analyzer)
{
    this->analyzer = analyzer;
}

QString RtScheduler::schedulingAnalysis(QString &latex) const
{
    if (analyzer) {
        return analyzer->text(this->tasks,latex);
    }
    return "No Scheduling analysis done";
}

RtScheduling *RtScheduler::getScheduling() const
{
    return schedulingResult;
}

void RtScheduler::setTaskSet(const RtTaskSet *taskset)
{
    this->tasks = taskset;
    currentTaskList = taskset->getTasksList();
    activeTasks.clear();
}

const RtTaskSet * RtScheduler::getTaskSet() const
{
    return tasks;
}

int RtScheduler::getAnalysisPeriod()
{
    return analysisPeriod;
}

int ppcm(int X,int Y)
{
    int A=X;
    int B=Y;
    while (A!=B)
    {
        while (A>B) B=B+Y;
        while (A<B) A=A+X;
    }
    return A;
}

int RtScheduler::calculateAnalysisPeriod()
{
    int periodMin;
    if (tasks->size() < 1) {
        analysisPeriod = 0;
        return 0;
    }
    periodMin = tasks->getTask(0)->period;
    int maxAperiodicStart = tasks->getTask(0)->start;
    for(int i=1;i<tasks->size();i++) {
        if (tasks->getTask(i)->isPeriodic())
            periodMin = ppcm(periodMin,tasks->getTask(i)->period);
        maxAperiodicStart = qMax(maxAperiodicStart, tasks->getTask(i)->start);
    }
    if (maxAperiodicStart != 0)
        analysisPeriod = 2 * periodMin + maxAperiodicStart;
    else
        analysisPeriod = periodMin;
    return analysisPeriod;
}

ActiveTask::ActiveTask(RtTask *task, int start)
{
    this->taskDesc = task;
    this->start = start;
    this->currentPriority = task->priority;
    this->residualCapacity = task->capacity;
}

ActiveTask *RtScheduler::findActiveServer(ActiveTask *activeTask)
{

    auto *aper = dynamic_cast<RtTaskAperiodic *>(activeTask->taskDesc);
    if (!aper)
        return nullptr;
    foreach(ActiveTask *task, activeTasks) {
        if (aper) {
            if (task->taskDesc->name == aper->serverName)
                return task;
        }
    }
    return nullptr;
}


int ActiveTask::getResidualCapacity() const
{
    if (taskDesc->isServer()) {
        int sum = 0;
        for(auto subTask : subTasks)
            sum += subTask->getResidualCapacity();
        return std::min(sum,residualCapacity);
    }
    return residualCapacity;
}

void ActiveTask::setResidualCapacity(int capacity)
{
    residualCapacity = capacity;
}

bool ActiveTask::isEligible() const
{
    if (taskDesc->isPurePeriodic())
        return true;
    if (taskDesc->isAperiodic())
        return false;
    // Then it is a server

    if (getResidualCapacity() > 0)
        return true;

    return false;

}

#include <iostream>

int RtScheduler::updateActiveTaskList()
{
    if (currentSlot == 0) {
        foreach(RtTask *task, currentTaskList) {
            if (task->isServer()) {
                if (currentSlot == 0) {
                    auto *newTask = new ActiveTask(task,currentSlot);
                    activeTasks.append(newTask);
                }
            }
        }
    }
    else {
        foreach(ActiveTask *task, activeTasks) {
            if (task->taskDesc->isServer()) {
                if (task->taskDesc->isPeriodic()) {
                    if ((task->taskDesc->start + currentSlot) % task->taskDesc->period == 0) {
                        task->setResidualCapacity(task->getResidualCapacity() + task->taskDesc->capacity);
                    }
                }
            }
        }
    }
    foreach(RtTask *task, currentTaskList) {
        if (task->isPurePeriodic()) {
            if (currentSlot >= task->start) {
                if ((currentSlot - task->start) % task->period == 0) {
                    activeTasks.append(new ActiveTask(task,currentSlot));
                }
            }
        }
        else if (task->isAperiodic()) {
            auto *aper = dynamic_cast<RtTaskAperiodic *>(task);
            for(int startTime : qAsConst(aper->startTimes)) {
                if (startTime == currentSlot) {
                    auto *newTask = new ActiveTask(task,currentSlot);
                    ActiveTask *serverTask = findActiveServer(newTask);
                    if (serverTask)
                        serverTask->subTasks.append(newTask);
                    activeTasks.append(newTask);
                }
            }
        }
    }

//    std::cout << "Number of active tasks: " << activeTasks.size() << std::endl;
    return 0;
}

void RtScheduler::generateScheduling()
{
    for(int slot=0;slot<getAnalysisPeriod() - 1; slot++) {
        if ((scheduling[slot].activeTask != scheduling[slot+1].activeTask)
                && (scheduling[slot].activeTask !=nullptr) && (scheduling[slot+1].activeTask != nullptr)) {
            schedulingResult->switches.append(RtSwitch(slot+1,scheduling[slot].activeTask->index,
                                                       scheduling[slot+1].activeTask->index));
        }
    }

    for(int i=0;i<getTaskSet()->size();i++) {
        RtTask *task = getTaskSet()->getTask(i);
        RtTaskResult *taskResult = schedulingResult->taskResults.at(i);
        //if (task->isPurePeriodic() || task->isAperiodic()) {
        {
            int slot = 0;
            int currentCapacity = 0;

            taskResult->residualCapacityLines.append(RtResidual(0,0));
            for(int s=0;s<taskResult->residualCapacity.size();s++) {
                RtResidualSlot res= taskResult->residualCapacity[s];
                taskResult->residualCapacityLines.append(RtResidual(res.slot,
                                                                    res.prevalue));
                taskResult->residualCapacityLines.append(RtResidual(res.slot + 1,
                                                                    res.postvalue));
            }


            taskResult->residualLaxityLines.append(RtResidual(0,0));
            for(int s=0;s<taskResult->residualLaxity.size();s++) {
                RtResidualSlot res= taskResult->residualLaxity[s];
                taskResult->residualLaxityLines.append(RtResidual(res.slot,
                                                                  res.prevalue));
                taskResult->residualLaxityLines.append(RtResidual(res.slot + 1,
                                                                  res.postvalue));
            }

            while (slot < getAnalysisPeriod()) {
                if (task->isPurePeriodic()) {
                    if ((slot - task->start) % task->period == 0) {
                        currentCapacity += task->capacity;
                    }
                }
                else if (task->isAperiodic()) {
                    if (dynamic_cast<RtTaskAperiodic *>(task)->startTimes.contains(slot))
                        currentCapacity += task->capacity;
                }
                if (task == scheduling[slot].activeTask) {
                    int end = slot;
                    while ((end + 1 < getAnalysisPeriod()) && (scheduling[end+1].activeTask == task) && (end - slot + 1 < currentCapacity)) {
                        end ++;

                        if (task->isPurePeriodic()) {
                            if ((end - task->start) % task->period == 0) {
                                currentCapacity += task->capacity;
                            }
                        }
                        else if (task->isAperiodic()) {
                            if (dynamic_cast<RtTaskAperiodic *>(task)->startTimes.contains(end))
                                currentCapacity += task->capacity;
                        }

                    }
                    if ((currentCapacity == task->capacity) && (end - slot + 1 == currentCapacity))
                        taskResult->executions.append(RtBlock(task->index, slot, end, RtBlock::FULL));
                    else if (currentCapacity == end - slot + 1) {
                        taskResult->executions.append(RtBlock(task->index, slot, end, RtBlock::END));
                    }
                    else if (currentCapacity == task->capacity) {
                        taskResult->executions.append(RtBlock(task->index, slot, end, RtBlock::START));
                    }
                    else {
                        taskResult->executions.append(RtBlock(task->index, slot, end, RtBlock::MIDDLE));
                    }
                    currentCapacity -= (end - slot + 1);
                    slot = end + 1;
                }
                else {
                    if ((currentCapacity % task->capacity) != 0) {
                        int end = slot;
                        while ((end + 1 < getAnalysisPeriod()) && (scheduling[end+1].activeTask != task)) {
                            end ++;

                            if (task->isPurePeriodic()) {
                                if ((end - task->start) % task->period == 0) {
                                    currentCapacity += task->capacity;
                                }
                            }
                            else if (task->isAperiodic()) {
                                if (dynamic_cast<RtTaskAperiodic *>(task)->startTimes.contains(end))
                                    currentCapacity += task->capacity;
                            }


                        }
                        taskResult->executions.append(RtBlock(task->index, slot, end, RtBlock::SUSPENDED));
                        slot = end + 1;
                    }
                    else
                        slot ++;
                }
            }
            taskResult->residualCapacityLines.append(RtResidual(slot,0));


        }
            if (task->isPeriodic()) {
                int occurence = 0;
                bool first = true;
                while (task->start + occurence * task->period <= getAnalysisPeriod()) {
                    if ((task->period == task->deadline) && (!first))
                        taskResult->requests.append(RtRequest(task->start + occurence * task->period,RtRequest::WHAT::STARTDEAD));
                    else
                        taskResult->requests.append(RtRequest(task->start + occurence * task->period,RtRequest::WHAT::START));
                    first = false;
                    occurence ++;
                }

                if (task->period != task->deadline) {
                    int occurence = 0;
                    while (task->start + occurence * task->period + task->deadline <= getAnalysisPeriod()) {
                        taskResult->requests.append(RtRequest(task->start + occurence * task->period + task->deadline,RtRequest::WHAT::DEADLINE));
                        occurence ++;
                    }
                }
        }
    }
}

#include <iostream>

void RtScheduler::printTasks()
{
    foreach(RtTask *task, currentTaskList) {
        std::cout << "Task " << qPrintable(task->name) << " : " << std::endl;
        std::cout << "Period : " << task->period << std::endl;
        std::cout << "Capacity : " << task->capacity << std::endl;
        std::cout << "Start : " << task->start << std::endl;
        std::cout << "Priority : " << task->priority << std::endl;
        std::cout << std::endl;
    }
}

void RtScheduler::clearScheduling()
{
    scheduling.clear();
    while (!activeTasks.isEmpty())
        delete activeTasks.takeFirst();
}



bool IndexSort(RtTask *task1, RtTask *task2)
{
    return task1->index < task2->index;
}

void RtScheduler::sortTasksPerIndex()
{
    qSort(currentTaskList.begin(), currentTaskList.end(), IndexSort);
}

bool PrioritySort(ActiveTask *task1, ActiveTask *task2)
{
    return task1->currentPriority > task2->currentPriority;
}



void RtScheduler::initSchedulingResult()
{
    
        delete schedulingResult;
    schedulingResult = new RtScheduling();
    for(int i=0;i<getTaskSet()->size();i++) {
        RtTask *task = getTaskSet()->getTask(i);
        auto *taskResult = new RtTaskResult;
        taskResult->task = task;
        taskResult->index = task->index;
        schedulingResult->taskResults.append(taskResult);
    }
}


void RtScheduler::printScheduling()
{
    for(auto task : qAsConst(currentTaskList)) {
        for(int slot = 0;slot < analysisPeriod; slot++) {
            Slot s = scheduling.at(slot);
            if (s.activeTask == task) {
                std::cout << "X";
            }
            else {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }
}


int RtScheduler::schedule()
{
    clearScheduling();
    initSchedulingResult();

    currentSlot = 0;

    calculateAnalysisPeriod();
    for(int i=0;i<analysisPeriod;i++) {
        updateActiveTaskList();
        assignPriorities();
        // Sort the active tasks in priority order
        qSort(activeTasks.begin(), activeTasks.end(), PrioritySort);

        // std::cout << "Slot: " << currentSlot << std::endl;
        // for(int t=0;t<activeTasks.size();t++)
        //     std::cout << "Task: " << qPrintable(activeTasks.at(t)->taskDesc->name) << std::endl;
        if (!activeTasks.empty()) {
            ActiveTask *activeTask = activeTasks.at(0);
            assignCurrentTask(i,activeTask);
        }
        else {
            assignCurrentTask(i,nullptr);
        }
        currentSlot ++;
    }

    generateScheduling();

    return 0;
}


void RtScheduler::assignCurrentTask(int slot,ActiveTask *task)
{
    if (task)
        scheduling.append(Slot(Slot::START,task->taskDesc));
    else
        scheduling.append(Slot(Slot::NOTHING,nullptr));

    for(int i=0;i<this->tasks->size();i++) {
        RtTask *realTask = tasks->getTask(i);
        bool found = false;
        ActiveTask *firstTask = nullptr;
        foreach(ActiveTask *t,this->activeTasks) {
            if (realTask == t->taskDesc) {
                found = true;
                if (firstTask) {
                    if (t->start < firstTask->start)
                        firstTask = t;
                }
                else
                    firstTask = t;
            }
        }
        if (found) {
            if (firstTask != task) {
                schedulingResult->taskResults.at(firstTask->taskDesc->index)->residualCapacity.append(RtResidualSlot(slot,firstTask->getResidualCapacity(),firstTask->getResidualCapacity()));
                schedulingResult->taskResults.at(firstTask->taskDesc->index)->residualLaxity.append(
                            RtResidualSlot(slot,firstTask->start + firstTask->taskDesc->deadline - slot - firstTask->getResidualCapacity(),
                                           firstTask->start + firstTask->taskDesc->deadline - slot - firstTask->getResidualCapacity() - 1));

            }
            else {
                schedulingResult->taskResults.at(firstTask->taskDesc->index)->residualCapacity.append(RtResidualSlot(slot,firstTask->getResidualCapacity(),firstTask->getResidualCapacity() - 1));
                schedulingResult->taskResults.at(firstTask->taskDesc->index)->residualLaxity.append(
                            RtResidualSlot(slot,firstTask->start + firstTask->taskDesc->deadline - slot - firstTask->getResidualCapacity(),
                                           firstTask->start + firstTask->taskDesc->deadline - slot - firstTask->getResidualCapacity()));

            }
        }
        else {
            RtResidualSlot lastCapa(slot,0,0);
            if (!schedulingResult->taskResults.at(realTask->index)->residualCapacity.isEmpty())
                lastCapa = schedulingResult->taskResults.at(realTask->index)->residualCapacity.last();
            else
                lastCapa = RtResidualSlot(slot,0,0);
            schedulingResult->taskResults.at(realTask->index)->residualCapacity.append(
                        RtResidualSlot(slot,lastCapa.postvalue,lastCapa.postvalue));

            schedulingResult->taskResults.at(realTask->index)->residualLaxity.append(
                        RtResidualSlot(slot,0,0));
        }
    }

    if (task) {
        task->setResidualCapacity(task->getResidualCapacity() - 1);
        if ((task->getResidualCapacity() == 0) && (!task->taskDesc->isServer()))
            activeTasks.removeOne(task);
    }
}
