/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "rttaskset.h"

RtTaskSet::RtTaskSet()
= default;

RtTask *RtTaskSet::getTask(int index) const
{
    for(auto i : tasksList)
        if (i->index == index)
            return i;
    return nullptr;
}

bool RtTaskSet::isValid() const
{
    foreach(RtTask *task, tasksList) {
        if (!task->isValid())
            return false;
    }
    return true;
}

void RtTaskSet::addTask(RtTask *task)
{
    tasksList.append(task);
    calculateIndexes();
}

void RtTaskSet::removeTask(RtTask *task)
{
    tasksList.removeOne(task);
    calculateIndexes();
}

void RtTaskSet::removeTask(int index)
{
    for(int i=0;i<size();i++)
        if (tasksList.at(i)->index == index) {
            tasksList.removeOne(tasksList.at(i));
            calculateIndexes();
            return;
        }
}

void RtTaskSet::restoreTask(RtTask *task)
{
    tasksList.insert(task->index,task);
}

void RtTaskSet::calculateIndexes()
{
    for(int i=0;i<size();i++)
        tasksList.at(i)->index = i;
}

QList<RtTask *> RtTaskSet::getTasksList() const
{
    return tasksList;
}

int RtTaskSet::size() const
{
    return tasksList.size();
}
