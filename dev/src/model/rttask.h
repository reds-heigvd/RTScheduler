/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef TASK_H
#define TASK_H

#include <QString>
#include <QList>
#include <QMap>


class RtTask
{
public:
    RtTask();
    RtTask(QString name,int capacity, int deadline, int period, int start, int priority = 0);

    virtual bool isPurePeriodic() const;
    virtual bool isAperiodic() const;
    virtual bool isServer() const;
    virtual bool isPeriodic() const;
    bool isValid() const;

    QString name;
    int capacity;
    int period;
    int deadline;
    int start;
    int priority;
    int index;
};


class RtTaskServer;

class RtTaskAperiodic : public RtTask
{
public:

    virtual bool isPurePeriodic() const;
    virtual bool isAperiodic() const;
    virtual bool isPeriodic() const;
    QList<int> startTimes;
    RtTaskServer *server;
    QString serverName;
};


class RtTaskServer : public RtTask
{
public:

    virtual bool isServer() const;
    virtual bool isPurePeriodic() const;
    virtual bool isPeriodic() const;

    enum class SERVER_TYPE {
        BACKGROUND = 0,
        POLLING,
        DEFFERABLE,
        SPORADIC,
        UNKNOWN};

    static QMap<SERVER_TYPE,QString> nameMap;

    RtTaskServer();
    RtTaskServer(const QString &name,int capacity, int period, int start, SERVER_TYPE serverType, int priority = 0);

    bool setServerType(const QString &serverTypeName);

    SERVER_TYPE serverType;
};

#endif // TASK_H
