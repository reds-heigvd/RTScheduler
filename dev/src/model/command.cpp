/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "command.h"



AddTask::AddTask(RtProject *project, QString taskName)
    : project(project), taskName(std::move(taskName)) { }

void AddTask::undo()
{
    project->getTaskSet()->removeTask(task);
    emit project->taskSetChanged();
}

void AddTask::redo()
{
    task = new RtTask();
    task->name = taskName;
    project->getTaskSet()->addTask(task);
    emit project->taskSetChanged();
}

RemoveTask::RemoveTask(RtProject *project, RtTask *task)
    : project(project), task(task) { }

#include <iostream>
#include <utility>

void RemoveTask::undo()
{
    project->getTaskSet()->restoreTask(task);
    std::cout << "Undo: " << project->getTaskSet()->size() << std::endl;
    emit project->taskSetChanged();
}

void RemoveTask::redo()
{
    project->getTaskSet()->removeTask(task);
    std::cout << "Redo: " << project->getTaskSet()->size() << std::endl;
    emit project->taskSetChanged();
}

ModifyTask::ModifyTask(RtProject *project,int index, RtTask  newValues) :
    project(project), index(index), newValues(std::move(newValues)) { }

void ModifyTask::undo()
{
    *project->getTaskSet()->getTask(index) = oldValues;
    emit project->singleTaskChanged(project->getTaskSet()->getTask(index));
}

void ModifyTask::redo()
{
    oldValues = *project->getTaskSet()->getTask(index);
    *project->getTaskSet()->getTask(index) = newValues;
    emit project->singleTaskChanged(project->getTaskSet()->getTask(index));
}
