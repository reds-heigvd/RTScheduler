/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "rtproject.h"
#include "rtschedulerfactory.h"

RtProject::RtProject() : taskSet(nullptr), modified(false)
{
    taskSet = new RtTaskSet();
}

RtProject::~RtProject()
{
    delete taskSet;
    while (!schedulerSet.isEmpty()) {
        RtScheduler *scheduler = schedulerSet.takeFirst();
        delete scheduler;
    }
}


void RtProject::setFileName(const QString &fileName)
{
    this->fileName = fileName;
}

QString RtProject::getFileName() const
{
    return fileName;
}

void RtProject::setModified(bool modified)
{
    this->modified = modified;
}

bool RtProject::isModified() const
{
    return modified;
}

#include "connect.h"
void RtProject::setTaskSet(RtTaskSet *set)
{
    delete taskSet;
    taskSet = set;

    emit taskSetChanged();
}

RtTaskSet *RtProject::getTaskSet() const
{
    return taskSet;
}

void RtProject::schedule()
{
    foreach(RtScheduler *scheduler, schedulerSet) {
        scheduler->setTaskSet(taskSet);
        scheduler->schedule();
    }
}

RtProject *RtProject::createNewProject()
{
    auto *project = new RtProject();
    project->schedulerSet.append(RtSchedulerFactory::createScheduler("RM"));
    project->schedulerSet.append(RtSchedulerFactory::createScheduler("DM"));
    project->schedulerSet.append(RtSchedulerFactory::createScheduler("EDF"));
    project->schedulerSet.append(RtSchedulerFactory::createScheduler("LLF"));
    return project;
}
