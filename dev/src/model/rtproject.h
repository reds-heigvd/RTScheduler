/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef RTPROJECT_H
#define RTPROJECT_H

#include "rttaskset.h"
#include "rtscheduler.h"

class RtProject : public QObject
{
    Q_OBJECT
public:
    RtProject();
    virtual ~RtProject();

    static RtProject *createNewProject();

    void setTaskSet(RtTaskSet *set);

    RtTaskSet *getTaskSet() const;
    QList<RtScheduler *> schedulerSet;

    void setFileName(const QString &fileName);
    QString getFileName() const;


    void setModified(bool modified = true);
    bool isModified() const;

    void schedule();

protected:

    //! The task set of the project
    RtTaskSet *taskSet;

    //! The name of the file the project belongs to. Initally empty for new projects
    QString fileName;

    //! A Flag indicating if the project was modified since the last save action
    bool modified;

signals:

    //! Emitted when a task is added or removed from the taskset
    void taskSetChanged();

    //! Emitted when a single task of the taskset is modified
    void singleTaskChanged(RtTask *task);

};

#endif // RTPROJECT_H
