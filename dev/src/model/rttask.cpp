/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "rttask.h"

#include <utility>

RtTask::RtTask() :
    name(""),capacity(0),period(0),deadline(0),start(0),priority(0),index(0)
{
}

RtTask::RtTask(QString name, int capacity, int deadline, int period, int start, int priority) :
    name(std::move(name)),
    capacity(capacity),
    period(period),
    deadline(deadline),
    start(start),
    priority(priority),
    index(0)
{

}


bool RtTask::isPurePeriodic() const
{
    return true;
}

bool RtTask::isAperiodic() const
{
    return false;
}

bool RtTask::isServer() const
{
    return false;
}

bool RtTask::isPeriodic() const
{
    return period != 0;
}

bool RtTaskServer::isServer() const
{
    return true;
}

bool RtTaskServer::isPurePeriodic() const
{
    return false;
}

bool RtTaskServer::isPeriodic() const
{
    switch (serverType) {
    case SERVER_TYPE::POLLING:
    case SERVER_TYPE::DEFFERABLE: return true;
    case SERVER_TYPE::BACKGROUND :
    case SERVER_TYPE::SPORADIC :
    case SERVER_TYPE::UNKNOWN : return false;
    }
    return false;
}

bool RtTaskAperiodic::isPeriodic() const
{
    return false;
}

bool RtTaskAperiodic::isPurePeriodic() const
{
    return false;
}

bool RtTaskAperiodic::isAperiodic() const
{
    return true;
}

bool RtTask::isValid() const
{
    if (period <= 0)
        return false;
    if (start < 0)
        return false;
    if (deadline <= 0)
        return false;
    if (capacity <= 0)
        return false;
    return true;
}


RtTaskServer::RtTaskServer() : serverType(SERVER_TYPE::UNKNOWN)
{
    static bool first = true;
    if (first) {
        first = false;
        nameMap.insert(SERVER_TYPE::BACKGROUND,"background");
        nameMap.insert(SERVER_TYPE::POLLING,"scrutation");
        nameMap.insert(SERVER_TYPE::DEFFERABLE,"deffered");
        nameMap.insert(SERVER_TYPE::SPORADIC,"sporadic");
    }
}

RtTaskServer::RtTaskServer(const QString &name,int capacity, int period, int start, SERVER_TYPE serverType, int priority) :
    RtTask(name,capacity,0,period,start,priority), serverType(serverType)
{

}


bool RtTaskServer::setServerType(const QString &serverTypeName)
{
    SERVER_TYPE type = nameMap.key(serverTypeName,SERVER_TYPE::UNKNOWN);
    if (type == SERVER_TYPE::UNKNOWN)
        return false;
    serverType = type;
    return true;
}


QMap<RtTaskServer::SERVER_TYPE,QString> RtTaskServer::nameMap;
