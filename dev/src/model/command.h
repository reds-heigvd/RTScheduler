/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef COMMAND_H
#define COMMAND_H

#include <QUndoCommand>

#include "rtproject.h"

class AddTask : public QUndoCommand
{
public:
    AddTask(RtProject *project, QString taskName);

    virtual void undo();

    virtual void redo();

private:
    RtProject *project{nullptr};
    RtTask *task{nullptr};
    QString taskName;
};


class RemoveTask : public QUndoCommand
{
public:
    RemoveTask(RtProject *project, RtTask *task);

    virtual void undo();

    virtual void redo();

private:
    RtProject *project;
    RtTask *task;
};

class ModifyTask : public QUndoCommand
{
public:
    ModifyTask(RtProject *project, int index, RtTask  newValues);

    virtual void undo();

    virtual void redo();

private:
    RtProject *project;
    int index;
    RtTask oldValues;
    RtTask newValues;
};


#endif // COMMAND_H
