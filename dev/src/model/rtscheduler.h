/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "rttask.h"
#include <QList>
#include <QTextStream>

#include "rttaskset.h"
#include "rtscheduling.h"


#define SCHEDULER_UTILS(idParam,nameParam) \
    public: \
    static QString idStatic() { \
        return idParam; \
    } \
    virtual QString id() const { \
        return idStatic(); \
    } \
    static QString nameStatic() { \
        return nameParam; \
    } \
    virtual QString name() const { \
        return nameStatic(); \
    } \


class Slot
{
public:
    typedef enum {NOTHING,START,END,STARTMIDDLE,ENDMIDDLE} WHAT;
    Slot(WHAT what, RtTask *activeTask = 0);
    WHAT what;
    RtTask *activeTask;
};

class ActiveTask
{
public:
    ActiveTask(RtTask *task, int start);
    virtual ~ActiveTask() {};

    RtTask *taskDesc;
    int start;
    int currentPriority;

    virtual int getResidualCapacity() const;
    void setResidualCapacity(int capacity);

    virtual bool isEligible() const;

    QList<ActiveTask *> subTasks;

protected:
    int residualCapacity;
};

class SchedulingAnalyzer
{
public:
    virtual bool schedulable(const RtTaskSet *taskset) = 0;
    virtual QString text(const RtTaskSet *taskset, QString &latex) = 0;
};


class RtScheduler
{
public:
    RtScheduler(SchedulingAnalyzer *analyzer = 0);
    virtual ~RtScheduler();

    void setSchedulingAnalyzer(SchedulingAnalyzer *analyzer);
    QString schedulingAnalysis(QString &latex) const;

    virtual QString id() const = 0;
    virtual int schedule();
    virtual void assignPriorities() = 0;
    virtual QString name() const = 0;
    virtual int calculateAnalysisPeriod();
    virtual int updateActiveTaskList();
    int getAnalysisPeriod();

    void setTaskSet(const RtTaskSet *taskset);
    const RtTaskSet *getTaskSet() const;

    virtual void printScheduling();

    void sortTasksPerIndex();

    void printTasks();


    void assignCurrentTask(int slot,ActiveTask *task);

    ActiveTask *findActiveServer(ActiveTask *activeTask);

    RtScheduling *getScheduling() const;

    QList<Slot> scheduling;

    int currentSlot{-1};

protected:

    virtual void generateScheduling();
    virtual void initSchedulingResult();

    const RtTaskSet *tasks{nullptr};

    RtScheduling *schedulingResult{nullptr};
    SchedulingAnalyzer *analyzer{nullptr};

    QList<RtTask *> currentTaskList;
    QList<ActiveTask *> activeTasks;


    virtual void clearScheduling();

    int analysisPeriod{-1};
};


bool PrioritySort(ActiveTask *task1, ActiveTask *task2);

int ppcm(int X,int Y);

#endif // SCHEDULER_H
