/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef RTTASKSET_H
#define RTTASKSET_H

#include <QObject>
#include "rttask.h"

class RtTaskSet
{
public:
    explicit RtTaskSet();

    bool isValid() const;

    void addTask(RtTask *task);
    void restoreTask(RtTask *task);
    void removeTask(RtTask *task);
    void removeTask(int index);
    void calculateIndexes();
    RtTask *getTask(int index) const;
    QList<RtTask *> getTasksList() const;
    int size() const;

protected:
    QList<RtTask*> tasksList;

signals:
    void taskSetChanged();

public slots:

};

#endif // RTTASKSET_H
