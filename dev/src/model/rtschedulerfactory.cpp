/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "rtschedulerfactory.h"

#include "rmscheduler.h"
#include "dmscheduler.h"
#include "edfscheduler.h"
#include "llfscheduler.h"
#include "fifoscheduler.h"



RtSchedulerFactory::RtSchedulerFactory()
= default;

RtScheduler* RtSchedulerFactory::createScheduler(const QString &id)
{
    if (id == "FIFO")
        return new FIFOScheduler();
    if (id == "RM")
        return new RMScheduler();
    if (id == "DM")
        return new DMScheduler();
    if (id == "EDF")
        return new EDFScheduler();
    if (id == "LLF")
        return new LLFScheduler();
    return nullptr;
}

QStringList RtSchedulerFactory::getAllIds()
{
    QStringList list;
    list << FIFOScheduler::idStatic() <<
            RMScheduler::idStatic()   <<
            DMScheduler::idStatic()   <<
            EDFScheduler::idStatic()  <<
            LLFScheduler::idStatic();
    return list;
}


QStringList RtSchedulerFactory::getAllNames()
{
    QStringList list;
    list << FIFOScheduler::nameStatic() <<
            RMScheduler::nameStatic()   <<
            DMScheduler::nameStatic()   <<
            EDFScheduler::nameStatic()  <<
            LLFScheduler::nameStatic();
    return list;
}
