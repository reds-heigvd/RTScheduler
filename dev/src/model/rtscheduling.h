/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef RTSCHEDULING_H
#define RTSCHEDULING_H

#include <QVector>

#include "rttask.h"

class RtResidual
{
public:
    RtResidual(int slot, int value) :
        slot(slot),value(value) {};

    int slot;
    int value;
};

class RtResidualSlot
{
public:
    RtResidualSlot(int slot, int prevalue, int postvalue) :
        slot(slot),prevalue(prevalue), postvalue(postvalue) {};

    int slot;
    int prevalue;
    int postvalue;
};

class RtRequest
{
public:
    enum class WHAT {START,DEADLINE,STARTDEAD};
    RtRequest(int slot,WHAT what) :
        slot(slot), what(what) {};
    int slot;
    WHAT what;
};

class RtSwitch
{
public:
    RtSwitch(int slot,int task1,int task2) :
        slot(slot), task1(task1), task2(task2) {};
    int slot;
    int task1;
    int task2;
};

class RtBlock
{
public:
    typedef enum {SUSPENDED,START,END,MIDDLE,FULL} WHAT;

    RtBlock(int task,int start,int stop,WHAT what) :
        task(task), start(start), stop(stop), what(what) {};
    int task;
    int start;
    int stop;
    WHAT what;
};

class RtTaskResult
{
public:
    RtTaskResult() : index(0),task(0) {};
    int index;
    RtTask *task;
    QList<RtResidualSlot> residualCapacity;
    QList<RtResidualSlot> residualLaxity;
    QList<RtResidual> residualCapacityLines;
    QList<RtResidual> residualLaxityLines;
    QList<RtBlock> executions;
    QList<RtRequest> requests;

};

class RtScheduling
{
public:
    RtScheduling();
    virtual ~RtScheduling()
    {
        foreach(RtTaskResult *taskResult, taskResults)
            delete taskResult;
    }

    QList<RtTaskResult*> taskResults;
    QList<RtSwitch> switches;
};

#endif // RTSCHEDULING_H
