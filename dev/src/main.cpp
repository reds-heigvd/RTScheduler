/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include <iostream>

#include <QFile>
#include <QCommandLineParser>
#include <QApplication>

#include "mainwindow.h"
#include "rtscheduler.h"
#include "rtprojectfilemanager.h"
#include "latexexporter.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCoreApplication::setApplicationName("RTScheduler");
    QCoreApplication::setApplicationVersion("1.0");


    QCommandLineParser parser;
    parser.setApplicationDescription("RTScheduler helper");
    parser.addHelpOption();
    parser.addVersionOption();

    // A boolean option with a single name (-c), for command line-only application
    QCommandLineOption commandLineOption("c", QCoreApplication::translate("main", "Command line (no GUI)"));
    parser.addOption(commandLineOption);

    // An option with a value
    QCommandLineOption taskFileOption(QStringList() << "i" << "taskfile",
                                      QCoreApplication::translate("main", "Loads the XML file <taskfile>."),
                                      QCoreApplication::translate("main", "taskfile"));
    parser.addOption(taskFileOption);


    // An option with a value
    QCommandLineOption latexFileOption(QStringList() << "latex" << "latexfile",
                                       QCoreApplication::translate("main", "Exports the scheduling in Latex format in file <latexfile>."),
                                       QCoreApplication::translate("main", "latexfile"));
    parser.addOption(latexFileOption);

    // Process the actual command line arguments given by the user
    parser.process(app);

    const QStringList args = parser.positionalArguments();

    QString taskFile = parser.value(taskFileOption);
    QString latexFile = parser.value(latexFileOption);


    bool commandLine = parser.isSet(commandLineOption);


    if (commandLine) {
        RtProjectFileManager fileManager;
        RtProject *project = fileManager.openProject(taskFile);
        if (!project) {
            std::cerr << qPrintable(fileManager.getErrorString()) << std::endl;
            return -2;
        }
        LatexExporter exporter;
        if (!exporter.exportToFile(latexFile,project)) {
            std::cerr << qPrintable(exporter.getErrorString()) << std::endl;
            return -3;
        }
        return 0;
    }

    MainWindow w;
    w.show();
    if (!taskFile.isEmpty())
        w.openFile(taskFile);

    return app.exec();
}
