/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <QSettings>
#include <QCoreApplication>

#define CONF_LASTFILEFOLDER "LastFileFolder"
#define CONF_LASTLATEXFOLDER "LastLatexFolder"

#define RT_APPLICATION_PATH    QCoreApplication::applicationDirPath()  //the directory that contains the application executable

//Configuration settings
// -- store settings in a local file
#define CONF_SETTINGS       RT_APPLICATION_PATH + "/settings.ini", QSettings::IniFormat


class AppSettings : public QSettings
{

private:
    AppSettings();
public:
    static AppSettings *getInstance();

};

#endif // APPSETTINGS_H
