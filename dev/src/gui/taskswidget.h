/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef TASKSWIDGET_H
#define TASKSWIDGET_H

#include <QTableWidget>
#include "rttask.h"
#include "rttaskset.h"

class TasksWidget : public QTableWidget
{
    Q_OBJECT
public:
    explicit TasksWidget(QWidget *parent = 0);

    void setTaskSet(RtTaskSet *tasks);

    RtTaskSet *taskSet;

    RtTaskSet *getTasks() const;

signals:
    void removeTask(int index);
    void singleTaskChanged(RtTask task);
public slots:

    void customHeaderMenuRequested(QPoint);
    void taskChanged(int row, int column);
    void updateTask(RtTask *task);
};

#endif // TASKSWIDGET_H
