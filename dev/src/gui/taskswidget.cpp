/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "taskswidget.h"

#include <QHeaderView>
#include <QMenu>

#include "connect.h"

TasksWidget::TasksWidget(QWidget *parent) :
    QTableWidget(parent), taskSet(nullptr)
{

    horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    CONNECT(horizontalHeader(), SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(customHeaderMenuRequested(QPoint)));

}

void TasksWidget::updateTask(RtTask *task)
{

    disconnect(this, SIGNAL(cellChanged(int,int)), this, SLOT(taskChanged(int,int)));
    this->horizontalHeaderItem(task->index)->setText(task->name);
    item(0,task->index)->setText(QString("%1").arg(task->capacity));
    item(1,task->index)->setText(QString("%1").arg(task->deadline));
    item(2,task->index)->setText(QString("%1").arg(task->period));
    item(3,task->index)->setText(QString("%1").arg(task->start));
    item(4,task->index)->setText(QString("%1").arg(task->priority));
    connect(this, SIGNAL(cellChanged(int,int)), this, SLOT(taskChanged(int,int)));
}

void TasksWidget::taskChanged(int /*row*/,int column)
{
    if (!taskSet)
        return;
    RtTask task(*taskSet->getTask(column));
    task.capacity = item(0,column)->text().toInt();
    task.deadline = item(1,column)->text().toInt();
    task.period = item(2,column)->text().toInt();
    task.start = item(3,column)->text().toInt();
    task.priority = item(4,column)->text().toInt();


    emit singleTaskChanged(task);
}

void TasksWidget::customHeaderMenuRequested(QPoint pos)
{
    QModelIndex index = this->indexAt(pos);
    auto *menu = new QMenu(this);
    auto *removeAct = new QAction(tr("Remove task"),this);
    menu->addAction(removeAct);
    QAction *action = menu->exec(this->viewport()->mapToGlobal(pos));
    if (action == removeAct) {
        emit removeTask(index.column());
    }
}


void TasksWidget::setTaskSet(RtTaskSet *tasks)
{
    disconnect(this, SIGNAL(cellChanged(int,int)), this, SLOT(taskChanged(int,int)));
    clear();
    this->setRowCount(5);
    this->setVerticalHeaderItem(0,new QTableWidgetItem(tr("Capacity")));
    this->setVerticalHeaderItem(1,new QTableWidgetItem(tr("Deadline")));
    this->setVerticalHeaderItem(2,new QTableWidgetItem(tr("Period")));
    this->setVerticalHeaderItem(3,new QTableWidgetItem(tr("Start")));
    this->setVerticalHeaderItem(4,new QTableWidgetItem(tr("Priority")));
    if (tasks)
        this->setColumnCount(tasks->size());
    else
        this->setColumnCount(0);

    if (tasks) {
        for(int i=0;i<tasks->size();i++) {
            RtTask *task = tasks->getTask(i);
            this->setHorizontalHeaderItem(task->index,new QTableWidgetItem( task->name));
            setItem(0,task->index, new QTableWidgetItem(QString("%1").arg(task->capacity)));
            setItem(1,task->index, new QTableWidgetItem(QString("%1").arg(task->deadline)));
            setItem(2,task->index, new QTableWidgetItem(QString("%1").arg(task->period)));
            setItem(3,task->index, new QTableWidgetItem(QString("%1").arg(task->start)));
            setItem(4,task->index, new QTableWidgetItem(QString("%1").arg(task->priority)));
        }
    }

    taskSet = tasks;

    resizeColumnsToContents();

    CONNECT(this, SIGNAL(cellChanged(int,int)), this, SLOT(taskChanged(int,int)));
}


RtTaskSet* TasksWidget::getTasks() const
{
    auto *tasks = new RtTaskSet();
    for(int i=0;i<columnCount();i++) {
        auto *task = new RtTask(horizontalHeaderItem(i)->text(),
                                  item(0,i)->text().toInt(),
                                  item(1,i)->text().toInt(),
                                  item(2,i)->text().toInt(),
                                  item(3,i)->text().toInt(),
                                  item(4,i)->text().toInt());
        tasks->addTask(task);
    }
    return tasks;
}
