/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/



#include <QGraphicsTextItem>

#include "schedulingscene.h"
#include "arrowitem.h"



#define LINEDIFF 50
#define SLOTHEIGHT 20
#define ARROWHEIGHT 30
#define RESIDUALHEIGHT 5


SchedulingScene::SchedulingScene(QObject *parent) :
    QGraphicsScene(parent), showCapacity(false),
    showLaxity(false),showTaskSlot(true)
{
    slotWidth = 20;
}

void SchedulingScene::internalUpdate()
{
    setProject(currentProject);
}

void SchedulingScene::setShowTaskSlot(bool show)
{
    showTaskSlot = show;
    internalUpdate();
}

void SchedulingScene::setShowCapacity(bool show)
{
    showCapacity = show;
    internalUpdate();
}

void SchedulingScene::setShowLaxity(bool show)
{
    showLaxity = show;
    internalUpdate();
}


QColor SchedulingScene::getColor(int index)
{
    const int MAXCOLOR = 4;
    std::array<QColor, MAXCOLOR> colors = {QColor(255,100,100),
                        QColor(100,255,100),
                        QColor(100,100,255),
                        QColor(255,255,100)};
    return colors[index % MAXCOLOR];
}

int SchedulingScene::relativeTaskYOffset(int index)
{
    return - index * LINEDIFF;
}


void SchedulingScene::addStartDead(double x, QGraphicsItem *parent)
{
    auto *item = new ArrowItem(parent,3);
    item->setLine(x*slotWidth,
                  0,
                  x*slotWidth,
                  - ARROWHEIGHT);
}

void SchedulingScene::addStart(double x, QGraphicsItem *parent)
{
    auto *item = new ArrowItem(parent,1);
    item->setLine(x*slotWidth,
                  0,
                  x*slotWidth,
                  - ARROWHEIGHT);
}

void SchedulingScene::addDead(double x, QGraphicsItem *parent)
{
    auto *item = new ArrowItem(parent,2);
    item->setLine(x*slotWidth,
                  0,
                  x*slotWidth,
                  - ARROWHEIGHT);
}

void SchedulingScene::drawRule(int index, QGraphicsItem *parent)
{
    auto *item = new ArrowItem(parent,1);
    item->setLine(0,0,(analysisPeriod+1) * slotWidth, 0);
    for(int i=0;i<analysisPeriod;i++) {
        if (i%5 == 0) {
            new QGraphicsLineItem(i*slotWidth,0,i*slotWidth,0 + 10,parent);
            if (index == 0) {
                auto *tItem = new QGraphicsTextItem(QString("%1").arg(i),parent);
                tItem->setPos(i*slotWidth-tItem->boundingRect().width()/2,0+10);
            }
        }
        else {
            new QGraphicsLineItem(i*slotWidth,0,i*slotWidth,0 + 5,parent);
        }
    }
}

void SchedulingScene::addSwitch(int slot,int index1,int index2, QGraphicsItem *parent)
{
    auto *line = new QGraphicsLineItem(slot*slotWidth,
                                                    relativeTaskYOffset(index1),
                                                    slot*slotWidth,
                                                    relativeTaskYOffset(index2),
                                                    parent);
    line->setPen(QPen(Qt::DotLine));
}

void SchedulingScene::addAlloc(int index, int startSlot, int duration, QGraphicsItem *parent)
{
    auto *item = new QGraphicsRectItem(startSlot * slotWidth,
                  0,
                  duration * slotWidth,
                  -SLOTHEIGHT,
                  parent);

    QRectF rectF = item->rect();
    QLinearGradient linearGrad(rectF.topLeft(), rectF.bottomRight());
    linearGrad.setColorAt(0, Qt::white);
    linearGrad.setColorAt(1, getColor(index));

    QBrush newBrush(linearGrad);
    item->setBrush(newBrush);
}


void SchedulingScene::addAllocStart(int index, int startSlot, int duration, QGraphicsItem *parent)
{
    addAlloc(index, startSlot, duration, parent);
}

void SchedulingScene::addAllocEnd(int index, int startSlot, int duration, QGraphicsItem *parent)
{
    addAlloc(index, startSlot, duration, parent);
}

void SchedulingScene::addAllocMiddle(int index, int startSlot, int duration, QGraphicsItem *parent)
{
    addAlloc(index, startSlot, duration, parent);
}

void SchedulingScene::addSuspended(int startSlot, int duration, QGraphicsItem *parent)
{
    auto *item = new QGraphicsRectItem(startSlot * slotWidth,
                  - (SLOTHEIGHT * 1)/4,
                  duration * slotWidth,
                  -SLOTHEIGHT / 2,
                                                    parent);

    QRectF rectF = item->rect();
    QLinearGradient linearGrad(rectF.topLeft(), rectF.bottomRight());
    linearGrad.setColorAt(0, Qt::white);
    linearGrad.setColorAt(1, QColor(100,100,100));

    QBrush newBrush(linearGrad);
    item->setBrush(newBrush);
    item->setPen(QPen(Qt::NoPen));
    item->setZValue(-1);
}

void SchedulingScene::setProject(const RtProject *project)
{
    this->clear();
    this->update();
    currentProject = project;
    if (project)
    {
        int offsetY = 0;
        foreach(RtScheduler *scheduler, project->schedulerSet) {
            analysisPeriod = scheduler->getAnalysisPeriod();
            drawAnalysisPeriod(analysisPeriod);
            scheduler->setTaskSet(project->getTaskSet());
            offsetY = drawScheduling(scheduler,offsetY);
        }
    }
}

constexpr double ZOOMFACTOR = 1.1;

void SchedulingScene::zoomIn()
{
    int newWidth = static_cast<int>(slotWidth * ZOOMFACTOR);
    if (newWidth == slotWidth)
        newWidth = slotWidth + 1;
    slotWidth = newWidth;
    internalUpdate();
}

void SchedulingScene::zoomOut()
{
    slotWidth = static_cast<int>(slotWidth / ZOOMFACTOR);
    if (slotWidth < 2)
        slotWidth = 2;
    internalUpdate();
}

#include <QGraphicsView>

void SchedulingScene::zoomFit()
{
    QGraphicsView *view = views().first();
    if (!view)
        return;
    QRect viewport_rect(0, 0, view->viewport()->width(), view->viewport()->height());

    if (!currentProject)
        return;
    if (!currentProject->getTaskSet())
        return;
    if (!currentProject->schedulerSet.first())
        return;
    int nameWidth = 0;
    for(int i=0;i<currentProject->getTaskSet()->size();i++) {
        RtTask *task = currentProject->getTaskSet()->getTask(i);
        QGraphicsTextItem *item = this->addText(task->name);
        int taskNameWidth = 10 + static_cast<int>(item->boundingRect().width());
        if (taskNameWidth > nameWidth)
            nameWidth = taskNameWidth;
        this->removeItem(item);
    }


    slotWidth = (viewport_rect.width()-nameWidth)/(currentProject->schedulerSet.at(0)->getAnalysisPeriod()+1) ;
    if (slotWidth<2)
        slotWidth = 2;
    internalUpdate();
}

void SchedulingScene::drawAnalysisPeriod(int period)
{

    auto *item = new QGraphicsTextItem(QString("Analysis period : %1").arg(period));
    addItem(item);
    item->setPos(-300,-100);
}

QGraphicsItem *SchedulingScene::drawTask(RtTaskResult *taskResult, bool showTaskSlot, bool showCapacity, bool showLaxity,
                              QGraphicsItem *parent)
{

    QGraphicsItem *taskItem = new QGraphicsItemGroup(parent);
    taskItem->setPos(0,relativeTaskYOffset(taskResult->index));
    taskItemList.append(taskItem);

    auto *item = new QGraphicsTextItem(taskResult->task->name, taskItem);
    item->setPos(-10-item->boundingRect().width(),-SLOTHEIGHT);


    if (showTaskSlot) {
        for(int b = 0;b<taskResult->executions.size();b++) {
            RtBlock block = taskResult->executions[b];
            switch(block.what) {
            case RtBlock::FULL: addAlloc(taskResult->index,block.start,block.stop - block.start + 1,taskItem);break;
            case RtBlock::END: addAllocEnd(taskResult->index,block.start,block.stop - block.start + 1,taskItem);break;
            case RtBlock::START: addAllocStart(taskResult->index,block.start,block.stop - block.start + 1,taskItem);break;
            case RtBlock::MIDDLE: addAllocMiddle(taskResult->index,block.start,block.stop - block.start + 1,taskItem);break;
            case RtBlock::SUSPENDED: addSuspended(block.start,block.stop - block.start + 1,taskItem);break;
            default: break;
            }
        }
    }

    for(auto request : qAsConst(taskResult->requests)) {
        switch(request.what) {
        case RtRequest::WHAT::START : addStart(request.slot, taskItem); break;
        case RtRequest::WHAT::DEADLINE : addDead(request.slot, taskItem); break;
        case RtRequest::WHAT::STARTDEAD : addStartDead(request.slot, taskItem); break;
        default: break;
        }
    }

    if (showCapacity)
    {
        QPolygonF polygon;
        for(auto & residualCapacityLine : taskResult->residualCapacityLines) {
            polygon.append(QPointF(residualCapacityLine.slot*slotWidth,
                                   -RESIDUALHEIGHT*residualCapacityLine.value));
        }
        QPainterPath path;
        path.addPolygon(polygon);


        auto *item = new QGraphicsPathItem(path,taskItem);
        QRectF rectF = item->boundingRect();
        QLinearGradient linearGrad(rectF.topLeft(), rectF.bottomRight());
        QColor c1= Qt::white;
        c1.setAlpha(100);
        QColor c2= getColor(taskResult->index);
        c2.setAlpha(100);
        linearGrad.setColorAt(0, c1);
        linearGrad.setColorAt(1, c2);

        QBrush newBrush(linearGrad);

        item->setBrush(newBrush);

    }


    if (showLaxity)
    {
        QPolygonF polygon;
        for(auto & residualLaxityLine : taskResult->residualLaxityLines) {
            polygon.append(QPointF(residualLaxityLine.slot*slotWidth,
                                   -RESIDUALHEIGHT*residualLaxityLine.value));
        }
        polygon.append(QPointF(taskResult->residualLaxityLines.last().slot*slotWidth,0));

        QPainterPath path;
        path.addPolygon(polygon);


        auto *item = new QGraphicsPathItem(path,taskItem);
        QRectF rectF = item->boundingRect();
        QLinearGradient linearGrad(rectF.topLeft(), rectF.bottomRight());
        QColor c1= Qt::white;
        c1.setAlpha(100);
        QColor c2= getColor(taskResult->index);
        c2.setAlpha(100);
        linearGrad.setColorAt(0, c1);
        linearGrad.setColorAt(1, c2);
        QBrush newBrush(linearGrad);
        item->setBrush(newBrush);

    }

    drawRule(taskResult->task->index,taskItem);

    return taskItem;
}

int SchedulingScene::drawScheduling(RtScheduler *scheduler, int offsetY)
{

    auto *schedulingItem = new QGraphicsItemGroup();
    addItem(schedulingItem);
    schedulingItem->setPos(0,offsetY);

    QString latex;
    auto *schedText = new QGraphicsTextItem(scheduler->schedulingAnalysis(latex));
    schedulingItem->addToGroup(schedText);
    schedText->setPos(-200,0);



    for(auto s : qAsConst(scheduler->getScheduling()->switches)) {
        addSwitch(s.slot,s.task1,s.task2, schedulingItem);
    }

    QRectF allTasksRect;

    for(auto taskResult : qAsConst(scheduler->getScheduling()->taskResults)) {
        QGraphicsItem *taskItem = drawTask(taskResult, true, showCapacity, showLaxity, schedulingItem);

        QRectF taskRect;
        foreach(QGraphicsItem *item,taskItem->childItems()) {
            QRectF rect = item->boundingRect();
            rect.translate(taskItem->pos());
            taskRect = taskRect.united(rect);
        }

        allTasksRect = allTasksRect.united(taskRect);
    }

    {
        auto *item = new QGraphicsTextItem(scheduler->name(),schedulingItem);
        item->setPos(0, allTasksRect.top() - 10);

//        allTasksRect = allTasksRect.united(item->boundingRect());
    }

    /*
    QGraphicsRectItem *re=new QGraphicsRectItem(allTasksRect,schedulingItem);
    re->setZValue(-1.0);
    re->setFlag(QGraphicsItem::ItemIsMovable,true);

    QRectF rectF = re->rect();
    QLinearGradient linearGrad(rectF.topLeft(), rectF.bottomRight());
    linearGrad.setColorAt(0, Qt::white);
    linearGrad.setColorAt(1, Qt::red);

    QBrush newBrush(linearGrad);
    re->setBrush(newBrush);
*/




    return offsetY + scheduler->getTaskSet()->size() * LINEDIFF + 50;
}
