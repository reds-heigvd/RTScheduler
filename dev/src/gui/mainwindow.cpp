/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "mainwindow.h"
#include <QDockWidget>
#include <QAction>
#include <QApplication>
#include <QMenuBar>
#include <QFileDialog>
#include <QToolBar>
#include <QInputDialog>
#include <QCloseEvent>

#include "command.h"
#include "appsettings.h"
#include "connect.h"
#include "rtprojectfilemanager.h"
#include "schedulingview.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), currentProject(nullptr)
{
    undoStack = new QUndoStack(this);

    view = new SchedulingView(this);
    view->setGeometry(0,0,1000,500);
//    view->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    scene = new SchedulingScene(this);
    view->setScene(scene);
    this->setCentralWidget(view);
    taskWidget = new TasksWidget(this);
    connect(taskWidget, SIGNAL(singleTaskChanged(RtTask)), this, SLOT(taskChanged(RtTask)));
    connect(taskWidget, SIGNAL(removeTask(int)), this, SLOT(removeTask(int)));
    auto *dockTasks = new QDockWidget(this);
    dockTasks->setWidget(taskWidget);
    this->addDockWidget(Qt::LeftDockWidgetArea, dockTasks);

    createActions();
    createMenus();
    createToolBars();


    readSettings();

}

void MainWindow::readSettings()
{
    QPoint pos = AppSettings::getInstance()->value("pos", QPoint(200, 200)).toPoint();
    QSize size = AppSettings::getInstance()->value("size", QSize(400, 400)).toSize();
    move(pos);
    resize(size);
}


void MainWindow::writeSettings() const
{
    AppSettings::getInstance()->setValue("pos", pos());
    AppSettings::getInstance()->setValue("size", size());
}

void MainWindow::quit()
{
    writeSettings();
    qApp->closeAllWindows();
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    quit();
    event->accept();
}

void MainWindow::createActions()
{

    exitAct = new QAction(QIcon::fromTheme("application-exit"), tr("E&xit"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    exitAct->setStatusTip(tr("Exit the application"));
    exitAct->setIcon(QIcon(":/resources/exit.png"));
    addAction(exitAct);
    CONNECT(exitAct, SIGNAL(triggered()), this, SLOT(quit()));

    printAct = new QAction(tr("Print"),this);
    printAct->setShortcut(QKeySequence::Print);
    printAct->setIcon(QIcon(":/resources/print.png"));
    addAction(printAct);
    CONNECT(printAct, SIGNAL(triggered()), this, SLOT(print()));

    openFileAct = new QAction(tr("Open file"),this);
    openFileAct->setShortcut(QKeySequence::Open);
    openFileAct->setIcon(QIcon(":/resources/open.png"));
//    openFileAct->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogOpenButton));
    addAction(openFileAct);
    CONNECT(openFileAct, SIGNAL(triggered()), this, SLOT(openFile()));


    saveFileAct = new QAction(tr("Save"),this);
    saveFileAct->setShortcut(QKeySequence::Save);
    saveFileAct->setIcon(QIcon(":/resources/save.png"));
//    saveFileAct->setIcon(qApp->style()->standardIcon(QStyle::SP_DialogSaveButton));

    addAction(saveFileAct);
    CONNECT(saveFileAct, SIGNAL(triggered()), this, SLOT(saveFile()));

    newFileAct = new QAction(tr("New file"),this);
    newFileAct->setShortcut(QKeySequence::New);
    newFileAct->setIcon(QIcon(":/resources/new.png"));
    addAction(newFileAct);
    CONNECT(newFileAct, SIGNAL(triggered()), this, SLOT(newFile()));

    saveFileAsAct = new QAction(tr("Save as"),this);
    saveFileAsAct->setShortcut(QKeySequence::SaveAs);
    addAction(saveFileAsAct);
    CONNECT(saveFileAsAct, SIGNAL(triggered()), this, SLOT(saveFileAs()));

    exportLatexAct = new QAction(tr("Export Latex"),this);
    exportLatexAct->setShortcut(tr("Ctrl+E"));
    addAction(exportLatexAct);
    CONNECT(exportLatexAct, SIGNAL(triggered()), this, SLOT(exportLatex()));


    aboutAct = new QAction(tr("About"),this);
    addAction(aboutAct);
    CONNECT(aboutAct, SIGNAL(triggered()), this, SLOT(about()));


    helpAct = new QAction(tr("Help"),this);
    helpAct->setShortcut(QKeySequence::HelpContents);
    addAction(helpAct);
    CONNECT(helpAct, SIGNAL(triggered()), this, SLOT(help()));

    addTaskAct = new QAction(tr("Add task"),this);
    addTaskAct->setIcon(QIcon(":/resources/add.png"));
    addTaskAct->setShortcut(Qt::CTRL + Qt::Key_Plus);
    addAction(addTaskAct);
    CONNECT(addTaskAct, SIGNAL(triggered()), this, SLOT(addTask()));

    viewTaskSlotAct = new QAction(tr("Show Tasks"),this);
    viewTaskSlotAct->setCheckable(true);
    viewTaskSlotAct->setChecked(true);
    addAction(viewTaskSlotAct);
    CONNECT(viewTaskSlotAct, SIGNAL(toggled(bool)), this, SLOT(viewTaskSlot(bool)));

    viewCapacityAct = new QAction(tr("Show capacity"),this);
    viewCapacityAct->setCheckable(true);
    addAction(viewCapacityAct);
    CONNECT(viewCapacityAct, SIGNAL(toggled(bool)), this, SLOT(viewCapacity(bool)));

    viewLaxityAct = new QAction(tr("Show laxity"),this);
    viewLaxityAct->setCheckable(true);
    addAction(viewLaxityAct);
    CONNECT(viewLaxityAct, SIGNAL(toggled(bool)), this, SLOT(viewLaxity(bool)));

    zoomInAct = new QAction(tr("Zoom in"),this);
    zoomInAct->setIcon(QIcon(":/resources/zoomin.png"));
    addAction(zoomInAct);
    CONNECT(zoomInAct, SIGNAL(triggered()), this, SLOT(zoomIn()));

    zoomOutAct = new QAction(tr("Zoom out"),this);
    zoomOutAct->setIcon(QIcon(":/resources/zoomout.png"));
    addAction(zoomOutAct);
    CONNECT(zoomOutAct, SIGNAL(triggered()), this, SLOT(zoomOut()));

    zoomFitAct = new QAction(tr("Zoom fit"),this);
    addAction(zoomFitAct);
    CONNECT(zoomFitAct, SIGNAL(triggered()), this, SLOT(zoomFit()));


    undoAct = undoStack->createUndoAction(this, tr("Undo"));
    undoAct->setShortcut(QKeySequence::Undo);
    undoAct->setIcon(QIcon(":/resources/undo.png"));
    addAction(undoAct);

    redoAct = undoStack->createRedoAction(this, tr("Redo"));
    redoAct->setShortcut(QKeySequence::Redo);
    redoAct->setIcon(QIcon(":/resources/redo.png"));
    addAction(redoAct);


}

void MainWindow::createMenus()
{

    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newFileAct);
    fileMenu->addAction(openFileAct);
    fileMenu->addAction(saveFileAct);
    fileMenu->addAction(saveFileAsAct);
//    fileMenu->addSeparator();
//    for (int i = 0; i < MaxRecentFiles; ++i)
//        fileMenu->addAction(recentFileActs[i]);
    fileMenu->addSeparator();
    fileMenu->addAction(exportLatexAct);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    QMenu *actionMenu = menuBar()->addMenu(tr("&Action"));
    actionMenu->addAction(undoAct);
    actionMenu->addAction(redoAct);
    actionMenu->addSeparator();
    actionMenu->addAction(addTaskAct);

    QMenu *viewMenu = menuBar()->addMenu(tr("&View"));
    viewMenu->addAction(viewTaskSlotAct);
    viewMenu->addAction(viewCapacityAct);
    viewMenu->addAction(viewLaxityAct);
    viewMenu->addAction(zoomInAct);
    viewMenu->addAction(zoomOutAct);
    viewMenu->addAction(zoomFitAct);

    QMenu *helpMenu = this->menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(helpAct);
    helpMenu->addAction(aboutAct);
}


void MainWindow::createToolBars()
{
    QToolBar *mainToolBar = addToolBar("Main");
    mainToolBar->addAction(newFileAct);
    mainToolBar->addAction(openFileAct);
    mainToolBar->addAction(saveFileAct);
    mainToolBar->addAction(undoAct);
    mainToolBar->addAction(redoAct);
    mainToolBar->addAction(addTaskAct);
    mainToolBar->addAction(zoomInAct);
    mainToolBar->addAction(zoomOutAct);
}

void MainWindow::viewTaskSlot(bool view)
{
    scene->setShowTaskSlot(view);

}

void MainWindow::viewCapacity(bool view)
{
    scene->setShowCapacity(view);

}

void MainWindow::viewLaxity(bool view)
{
    scene->setShowLaxity(view);
}

void MainWindow::zoomIn()
{
    scene->zoomIn();
}

void MainWindow::zoomOut()
{
    scene->zoomOut();
}

void MainWindow::zoomFit()
{
    scene->zoomFit();
}

void MainWindow::removeTask(int index)
{
    if (!currentProject) {
        QMessageBox::warning(this,tr("Error"),tr("Please create a project first"));
        return;
    }
    RtTask *task = currentProject->getTaskSet()->getTask(index);
    if (task) {
        undoStack->push(new RemoveTask(currentProject,task));
    }
}

void MainWindow::addTask()
{
    if (!currentProject) {
        QMessageBox::warning(this,tr("Error"),tr("Please create a project first"));
        return;
    }
    bool ok;
    QString taskName = QInputDialog::getText(this,tr("Task name"),tr("Task name"),QLineEdit::Normal,"Task 1", &ok);
    if (!ok)
        return;
    auto *command = new AddTask(currentProject, taskName);
    undoStack->push(command);
    updateProject();
}

void MainWindow::taskChanged(RtTask task)
{
    undoStack->push(new ModifyTask(currentProject,task.index,task));
//    updateProject(false);

    //RtScheduler *scheduler = new EDFScheduler();
    //scheduler->tasks = taskWidget->getTasks();

    //scheduler->schedule();
    //scene->setScheduler(scheduler);
}

MainWindow::~MainWindow()
{
    if (currentProject)
        delete currentProject;

}

void MainWindow::updateProject(bool propagate)
{
    if (currentProject) {
        if ((currentProject->getTaskSet() != nullptr) && (currentProject->getTaskSet()->isValid())) {
            currentProject->schedule();
            scene->setProject(currentProject);
        }
        else
            scene->setProject(nullptr);
    }
    if (currentProject) {
        if (propagate)
            taskWidget->setTaskSet(currentProject->getTaskSet());
    }
    else {
        taskWidget->clear();
        scene->setProject(nullptr);
    }
}


void MainWindow::openFile()
{
    if (currentProject) {
        // TODO : Ask if we close the current one

        delete currentProject;
    }

    //.rtp file
    QFileDialog dialog(this,tr("Open the taskset file"),
                       AppSettings::getInstance()->value(CONF_LASTFILEFOLDER,
                                                         QDir::currentPath()).toString(),
                       "Realtime project (*.rtp)");
    dialog.setFileMode(QFileDialog::ExistingFile);

    if (!dialog.exec())
        return;

    QFileInfo fileInfo(dialog.selectedFiles().at(0));
    AppSettings::getInstance()->setValue(CONF_LASTFILEFOLDER,fileInfo.absolutePath());
    //Load the XML
    openFile(dialog.selectedFiles().at(0));

}

void MainWindow::openFile(const QString &fileName)
{

    RtProjectFileManager fileOpener;
    RtProject *project = fileOpener.openProject(fileName);

    //If there was any errors, show them
    if (project == nullptr) {
        QMessageBox::warning(this,"Error in opening file",fileOpener.getErrorString());
    }

    currentProject = project;
    if (project) {
        CONNECT(currentProject, SIGNAL(taskSetChanged()), this, SLOT(updateProject()));
        CONNECT(currentProject, SIGNAL(singleTaskChanged(RtTask*)), taskWidget, SLOT(updateTask(RtTask*)));
        CONNECT(currentProject, SIGNAL(singleTaskChanged(RtTask*)), this, SLOT(updateTask(RtTask*)));
    }
    updateProject();
}

void MainWindow::updateTask(RtTask */*task*/)
{
    updateProject(false);
}

void MainWindow::newFile()
{
    if (currentProject) {
        // TODO Ask if we really create a new one

        delete currentProject;
    }
    currentProject = RtProject::createNewProject();

    if (currentProject) {
        CONNECT(currentProject, SIGNAL(taskSetChanged()), this, SLOT(updateProject()));
        CONNECT(currentProject, SIGNAL(singleTaskChanged(RtTask*)), taskWidget, SLOT(updateTask(RtTask*)));
        CONNECT(currentProject, SIGNAL(singleTaskChanged(RtTask*)), this, SLOT(updateTask(RtTask*)));
    }

    updateProject();

}

void MainWindow::saveFile()
{
    if (!currentProject)
        return;
    if (currentProject->getFileName().isEmpty()) {
        saveFileAs();
        return;
    }
    RtProjectFileManager fileSaver;
    if (!fileSaver.saveProject(currentProject->getFileName(),currentProject)) {
        QMessageBox::warning(this,"Error while saving file",fileSaver.getErrorString());
    }
}

void MainWindow::saveFileAs()
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Select file"),
                       AppSettings::getInstance()->value(CONF_LASTFILEFOLDER,
                                                         QDir::currentPath()).toString(),
                       "Realtime project (*.rtp)");
    if (fileName.isEmpty())
        return;
    if (fileName.right(4) != ".rtp")
        fileName += ".rtp";
    RtProjectFileManager fileSaver;
    if (!fileSaver.saveProject(fileName,currentProject)) {
        QMessageBox::warning(this,"Error while saving file",fileSaver.getErrorString());
    }

    QFileInfo fileInfo(fileName);
    AppSettings::getInstance()->setValue(CONF_LASTFILEFOLDER,fileInfo.absolutePath());
}

void MainWindow::print()
{

}

void MainWindow::about()
{

}

void MainWindow::help()
{

}

#include "latexexporter.h"

void MainWindow::exportLatex()
{
    if (currentProject == nullptr)
        return;
    //.rtp file

    QString fileName = QFileDialog::getSaveFileName(this,tr("Save file"),
                       AppSettings::getInstance()->value(CONF_LASTLATEXFOLDER,
                                                         QDir::currentPath()).toString(),
                       "Latex file (*.tex)");
    if (fileName.isEmpty())
        return;

    QFileInfo fileInfo(fileName);
    AppSettings::getInstance()->setValue(CONF_LASTLATEXFOLDER,fileInfo.absolutePath());

    LatexExporter exporter;
    exporter.exportToFile(fileName,currentProject);
}
