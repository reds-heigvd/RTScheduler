/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef SCHEDULINGSCENE_H
#define SCHEDULINGSCENE_H

#include <QGraphicsScene>
#include "rtscheduler.h"
#include "rtproject.h"

class SchedulingScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit SchedulingScene(QObject *parent = 0);

    void setProject(const RtProject *project);

    void setShowTaskSlot(bool show);
    void setShowCapacity(bool show);
    void setShowLaxity(bool show);
    void zoomIn();
    void zoomOut();
    void zoomFit();

protected:

    //! Redraw the schedulin. Call it after a change in the view option
    void internalUpdate();

    void drawAnalysisPeriod(int analysisPeriod);

    int drawScheduling(RtScheduler *scheduler, int offsetY);
    void addStartDead(double x, QGraphicsItem *parent);
    void addStart(double x, QGraphicsItem *parent);
    void addDead(double x, QGraphicsItem *parent);

    void addSwitch(int slot,int index1,int index2, QGraphicsItem *parent);

    void addAlloc(int index, int startSlot, int duration, QGraphicsItem *parent);
    void addAllocStart(int index, int startSlot, int duration, QGraphicsItem *parent);
    void addAllocEnd(int index, int startSlot, int duration, QGraphicsItem *parent);
    void addAllocMiddle(int index, int startSlot, int duration, QGraphicsItem *parent);
    void addSuspended(int startSlot, int duration, QGraphicsItem *parent);


    QGraphicsItem *drawTask(RtTaskResult *taskResult, bool showTaskSlot, bool showCapacity, bool showLaxity, QGraphicsItem *parent);

    void drawRule(int index, QGraphicsItem *parent);

    int relativeTaskYOffset(int index);
    int taskXOffset();
    QColor getColor(int index);

    const RtProject *currentProject{};

    int slotWidth;

    QList<QGraphicsItem *> taskItemList;

    bool showCapacity;
    bool showLaxity;
    bool showTaskSlot;
    int analysisPeriod{};

signals:

public slots:

};

#endif // SCHEDULINGSCENE_H
