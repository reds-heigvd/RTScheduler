/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QUndoStack>

#include "schedulingscene.h"
#include "taskswidget.h"
#include "rtproject.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QGraphicsView *view;
    SchedulingScene *scene;
    TasksWidget *taskWidget;

    void createActions();
    void createMenus();
    void createToolBars();

    QAction *newFileAct;
    QAction *openFileAct;
    QAction *saveFileAct;
    QAction *saveFileAsAct;
    QAction *exportLatexAct;
    QAction *exitAct;

    QAction *viewTaskSlotAct;
    QAction *viewCapacityAct;
    QAction *viewLaxityAct;
    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *zoomFitAct;

    QAction *printAct;

    QAction *undoAct;
    QAction *redoAct;
    QAction *addTaskAct;

    QAction *aboutAct;
    QAction *helpAct;

    RtProject *currentProject;
    void openFile(const QString &fileName);

protected:
    void readSettings();
    void writeSettings() const;


    QUndoStack *undoStack;
private slots:
    void openFile();
    void newFile();
    void saveFile();
    void saveFileAs();
    void print();
    void about();
    void help();
    void exportLatex();
    void quit();

    void viewTaskSlot(bool view);
    void viewCapacity(bool view);
    void viewLaxity(bool view);
    void zoomIn();
    void zoomOut();
    void zoomFit();

    void addTask();

    void updateProject(bool propagate=true);
    void closeEvent(QCloseEvent *);

public slots:
    void taskChanged(RtTask task);
    void removeTask(int index);
    void updateTask(RtTask *task);

};

#endif // MAINWINDOW_H
