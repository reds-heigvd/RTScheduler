/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "rtprojectfilemanager.h"

#include <QDomDocument>
#include <QFile>


#include "rtschedulerfactory.h"
#include "rttask.h"

#define XML_PROJECT_VERSION "1.0"

RtProjectFileManager::RtProjectFileManager()
= default;

RtProject *RtProjectFileManager::openProject(const QString &fileName)
{

    //Load the file
    QDomDocument doc=QDomDocument("RtProject");
    QFile f(fileName);
    if (!f.open(QIODevice::ReadOnly)) {
        errorString += QObject::tr("Could not open the project file %1\n").arg(fileName);
        return nullptr;
    } if (!doc.setContent(&f)) {
        f.close();
        errorString += QObject::tr("Could not read the project XML %1\n").arg(fileName);
        return nullptr;
    }
    f.close();

    //Verify the XML version
    QString version = doc.documentElement().attribute("version","0.0");
    if ( version != XML_PROJECT_VERSION ) {
        errorString += QObject::tr("The given XML has an invalid version (expected %1, got %2)\n").arg(XML_PROJECT_VERSION, version);
        return nullptr;
    }

    auto *project = new RtProject();
    project->setFileName(fileName);
    project->setTaskSet( new RtTaskSet());

    //Go through the sub-elements
    QDomNode n = doc.documentElement().firstChild();
    while(!n.isNull()) {
        //Should be an element
        QDomElement crt = n.toElement();

        if (crt.tagName() == "TaskSet") {
            QDomNode ntask = crt.firstChild();
            while (!ntask.isNull()) {
                QDomElement crtTask = ntask.toElement();

                //Name
                if (crtTask.tagName() == "Task") {
                    QString type = crtTask.attribute("type","periodic");
                    RtTask *task = nullptr;
                    RtTaskServer *serverTask = nullptr;
                    RtTaskAperiodic *aperiodicTask = nullptr;
                    if (type == "periodic")
                        task = new RtTask();
                    else if (type == "aperiodic") {
                        aperiodicTask = new RtTaskAperiodic();
                        task = aperiodicTask;
                    }
                    else if (type == "server") {
                        serverTask = new RtTaskServer();
                        task = serverTask;
                    }
                    else {
                        errorString += QObject::tr("The task type is unknown : %1").arg(type);
                        return nullptr;
                    }
                    QDomNode nn = crtTask.firstChild();
                    while(!nn.isNull()) {
                        if (nn.toElement().tagName() == "Name")
                            task->name = nn.firstChild().toText().data();
                        else if (nn.toElement().tagName() == "Capacity")
                            task->capacity = nn.firstChild().toText().data().toInt();
                        else if (nn.toElement().tagName() == "Period")
                            task->period = nn.firstChild().toText().data().toInt();
                        else if (nn.toElement().tagName() == "Deadline")
                            task->deadline = nn.firstChild().toText().data().toInt();
                        else if (nn.toElement().tagName() == "Priority")
                            task->priority = nn.firstChild().toText().data().toInt();
                        else if (nn.toElement().tagName() == "StartTime") {
                            if (aperiodicTask)
                                aperiodicTask->startTimes.append(nn.firstChild().toText().data().toInt());
                            else
                                task->start = nn.firstChild().toText().data().toInt();
                        }
                        else if (nn.toElement().tagName() == "ServerName") {
                            if (!aperiodicTask) {
                                errorString += QObject::tr("The tag ServerName should only be used within an aperiodic task");
                                return nullptr;
                            }
                            aperiodicTask->serverName = nn.firstChild().toText().data();
                        }
                        else if (nn.toElement().tagName() == "ServerType") {
                            auto *serverTask = dynamic_cast<RtTaskServer *>(task);
                            if (!serverTask) {
                                errorString += QObject::tr("The tag ServerType should only be used within a server task");
                                return nullptr;
                            }
                            if (!serverTask->setServerType(nn.firstChild().toText().data())) {
                                errorString += QObject::tr("The server type %1 is not supported").arg(nn.firstChild().toText().data());
                                return nullptr;
                            }
                        }
                        else
                            errorString += QString("Unknown tag: %1").arg(nn.toElement().tagName());
                        nn = nn.nextSibling();
                    }
                    if (aperiodicTask) {
                        if (aperiodicTask->serverName.isEmpty()) {
                            errorString += QObject::tr("The name of the server should be set for an aperiodic task");
                            return nullptr;
                        }
                    }
                    if (serverTask) {
                        if (serverTask->serverType == RtTaskServer::SERVER_TYPE::UNKNOWN) {
                            errorString += QObject::tr("The server type was not set properly");
                            return nullptr;
                        }
                    }


                    project->getTaskSet()->addTask(task);
                }
                else {
                    errorString += QString("Unkown tag: %1").arg(crtTask.tagName());
                }
                ntask = ntask.nextSibling();
            }
        }
        else if (crt.tagName() == "Schedulers") {
            QDomNode nscheduler = crt.firstChild();
            while (!nscheduler.isNull()) {
                QDomElement crtScheduler = nscheduler.toElement();

                //Name
                if (crtScheduler.tagName() == "Scheduler") {
                    QDomElement id = crtScheduler.firstChildElement("Id");
                    if (id.isNull()) {
                        errorString += QString("No scheduler ID tag");
                    }
                    else {
                        RtScheduler *scheduler = RtSchedulerFactory::createScheduler(id.firstChild().toText().data());
                        if (!scheduler)
                            errorString += QString("No scheduler corresponding to id %1").arg(id.firstChild().toText().data());
                        else
                            project->schedulerSet.append(scheduler);
                    }
                }
                else {
                    errorString += QString("Unkown tag: %1").arg(crtScheduler.tagName());
                }
                nscheduler = nscheduler.nextSibling();
            }
        }
        n = n.nextSibling();
    }

    if (!errorString.isEmpty())
    {
        return nullptr;
    }

    foreach(RtScheduler *scheduler,project->schedulerSet)
        scheduler->setTaskSet(project->getTaskSet());
    return project;
}

bool RtProjectFileManager::saveProject(const QString &fileName, RtProject *project)
{

    //Load the file
    QDomDocument doc=QDomDocument("RtProject");
    QFile f(fileName);
    if (!f.open(QIODevice::WriteOnly)) {
        errorString += QObject::tr("Could not open the project file %1\n").arg(fileName);
        return false;
    }

    doc.appendChild(doc.createProcessingInstruction("xml", "version='1.0' encoding='UTF-8'"));


    QDomElement root = doc.createElement("RtProject");
    doc.appendChild(root);

    root.setAttribute("version", XML_PROJECT_VERSION);

    QDomElement taskSet = doc.createElement("TaskSet");
    for(int i=0;i<project->getTaskSet()->size(); i++) {
        RtTask *task = project->getTaskSet()->getTask(i);
        QDomElement taskElement = doc.createElement("Task");
        taskSet.appendChild(taskElement);
        {
            QDomElement periodElement = doc.createElement("Name");
            taskElement.appendChild(periodElement);
            periodElement.appendChild(doc.createTextNode(task->name));
        }
        {
            QDomElement periodElement = doc.createElement("Capacity");
            taskElement.appendChild(periodElement);
            periodElement.appendChild(doc.createTextNode(QString("%1").arg(task->capacity)));
        }
        {
            QDomElement periodElement = doc.createElement("Period");
            taskElement.appendChild(periodElement);
            periodElement.appendChild(doc.createTextNode(QString("%1").arg(task->period)));
        }
        {
            QDomElement periodElement = doc.createElement("Deadline");
            taskElement.appendChild(periodElement);
            periodElement.appendChild(doc.createTextNode(QString("%1").arg(task->deadline)));
        }
        {
            QDomElement periodElement = doc.createElement("StartTime");
            taskElement.appendChild(periodElement);
            periodElement.appendChild(doc.createTextNode(QString("%1").arg(task->start)));
        }
    }

    root.appendChild(taskSet);

    QDomElement schedulerSet = doc.createElement("Schedulers");
    foreach (RtScheduler *scheduler, project->schedulerSet) {
        QDomElement schedulerElement = doc.createElement("Scheduler");
        schedulerSet.appendChild(schedulerElement);
        {
            QDomElement periodElement = doc.createElement("Id");
            schedulerElement.appendChild(periodElement);
            periodElement.appendChild(doc.createTextNode(scheduler->id()));
        }
    }
    root.appendChild(schedulerSet);

    QTextStream out(&f);
    out << doc.toString();
    f.close();

    project->setFileName(fileName);
    return true;
}

QString RtProjectFileManager::getErrorString() const
{
    return errorString;
}
