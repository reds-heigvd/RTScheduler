/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "latexexporter.h"

#include <QFile>

LatexExporter::LatexExporter()
= default;

LatexExporter::~LatexExporter()
= default;

bool LatexExporter::exportToFile(const QString &fileName, RtProject *project)
{
    errorString = "";
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        errorString += QString("Can not open Latex file %1 in write mode").arg(fileName);
        return false;
    }
    QTextStream out(&file);
    foreach(RtScheduler *scheduler, project->schedulerSet)
        exportLatex(out,scheduler);
    return true;
}


void LatexExporter::exportLatex(QTextStream &out, RtScheduler *scheduler)
{
    scheduler->sortTasksPerIndex();

    out << "Scheduling analysis for algorithm " << scheduler->name() << " : \\\\" << endl;
    QString latex;
    scheduler->schedulingAnalysis(latex);
    out << latex;

    out << "\\begin{center}" << endl;
    out << "\\begin{scheduling}{" << scheduler->getTaskSet()->size() << "}{" << scheduler->getAnalysisPeriod() << "}" << endl;

    out << "\\dimslotwidth=.7cm" << endl;
    for(int i=0;i<scheduler->getTaskSet()->size();i++)
        out << "\\schedtask{" << scheduler->getTaskSet()->getTask(i)->index << "}{$" << qPrintable(scheduler->getTaskSet()->getTask(i)->name) << "$}" << endl;
    for(int i=0;i<scheduler->getTaskSet()->size();i++) {
        RtTask *task = scheduler->getTaskSet()->getTask(i);
        int slot = 0;
        int currentCapacity = task->capacity;
        while (slot < scheduler->getAnalysisPeriod()) {
            if (task == scheduler->scheduling[slot].activeTask) {
                int end = slot;
                while ((end + 1 < scheduler->getAnalysisPeriod()) && (scheduler->scheduling[end+1].activeTask == task) && (end - slot + 1 < currentCapacity))
                    end ++;
                if ((currentCapacity == task->capacity) && (end - slot + 1 == currentCapacity))
                    out << "\\alloc{" << task->index << "}{" << slot << "}{" << end - slot + 1 << "}" << endl;
                else if (currentCapacity == end - slot + 1) {
                    out << "\\allocend{" << task->index << "}{" << slot << "}{" << end - slot + 1 << "}" << endl;
                    currentCapacity = task->capacity;
                }
                else if (currentCapacity == task->capacity) {
                    out << "\\allocstart{" << task->index << "}{" << slot << "}{" << end - slot + 1 << "}" << endl;
                    currentCapacity -= (end - slot + 1);
                }
                else {
                    out << "\\allocmiddle{" << task->index << "}{" << slot << "}{" << end - slot + 1 << "}" << endl;
                    currentCapacity -= (end - slot + 1);
                }
                slot = end + 1;
            }
            else {
                if (currentCapacity != task->capacity) {
                    int end = slot;
                    while ((end + 1 < scheduler->getAnalysisPeriod()) && (scheduler->scheduling[end+1].activeTask != task))
                        end ++;
                    out << "\\suspended{" << task->index << "}{" << slot << "}{" << end - slot + 1 << "}" << endl;
                    slot = end + 1;
                }
                else
                    slot ++;
            }
        }
    }

    for(int i=0;i<scheduler->getTaskSet()->size();i++) {
        RtTask *task = scheduler->getTaskSet()->getTask(i);
        int occurence = 0;
        bool first = true;
        while (task->start + occurence * task->period <= scheduler->getAnalysisPeriod()) {
            if ((task->period == task->deadline) && (!first))
                out << "\\startdead{";
            else
                out << "\\start{";
            out << task->index << "}{" << task->start + occurence * task->period << "}" << endl;
            first = false;
            occurence ++;
        }

        if (task->period != task->deadline) {
            int occurence = 0;
            while (task->start + occurence * task->period + task->deadline <= scheduler->getAnalysisPeriod()) {
                out << "\\deadl{" << task->index << "}{" << task->start + occurence * task->period + task->deadline<< "}" << endl;
                occurence ++;
            }
        }
    }

    out << "\\end{scheduling}" << endl;
    out << "\\end{center}" << endl;
}

QString LatexExporter::getErrorString() const
{
    return errorString;
}
