/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef RTPROJECTFILEMANAGER_H
#define RTPROJECTFILEMANAGER_H

#include <QString>
#include "rtproject.h"

/** Class responsible to open and save projects to files
 */
class RtProjectFileManager
{
public:

    //! Empty constructor
    RtProjectFileManager();

    /** Opens a file containing the description of an RtProject.
     * If it returns 0, then something went wrong, and getErrorString()
     * will indicate a message about the error.
     * @param fileName Name of the file to open
     * @return A pointer to the newly created project. 0 if something went wrong
     */
    RtProject* openProject(const QString &fileName);

    /** Saves an RtProject to a file.
     * If it returns false, then something went wrong, and getErrorString()
     * will indicate a message about the error.
     * @param fileName Name of the file where the project should be saved
     * @param project Project to be saved
     * @return true if everything went well, false else.
     */
    bool saveProject(const QString &fileName, RtProject *project);

    /** Returns the error message if something goes wrong
     * If openProject returned 0 or saveProject returned false, then
     * this function will return a message indicating what went wrong.
     * @return The message indicating the error
     */
    QString getErrorString() const;

protected:

    //! String containing the error. Should be emptied when starting a processing.
    QString errorString;
};

#endif // RTPROJECTFILEMANAGER_H
