/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include <cmath>

#include "schedulinganalyzer.h"



/*
class Rational{
    int num; // stocke le numérateur
    int den; // stocke le dénominateur
public:
        Rational(int a=0,int b=1); // le constructeur de la classe Q (le nombre 1 par défaut)
        Rational operator+(Rational a); // Définit l'opérateur +
        int ceil() const;
        //void show(); // Affiche le nombre rationnel
};

Rational::Rational(int a,int b){ // Le constructeur
        num=a;
        den=b;
}

Rational Rational::operator+(Rational a){
        Rational t; // crée un nouveau nombre rationnel
        t.num=num*a.den+den*a.num; // stocke le numérateur des nombres additionnés
        t.den=den*a.den;  // stocke le dénominateur des nombres additionnés.
        return t; // retourne le résultat de l'addition
}

int Rational::ceil() const {
    if (den == 1) {
        return num;
    }
    return (static_cast<int>(static_cast<double>(num)/static_cast<double>(den))) + 1;

}

//void Rational::show(){
//     std::cout << num << " / " << den << " = " << static_cast<double>(num)/static_cast<double>(den) << "\n";
//}
*/

bool prioritySort(RtTask *task1, RtTask *task2)
{
    return task1->priority > task2->priority;
}


QString interference(const RtTaskSet *taskset, QString &latex, bool &ok)
{
    ok = true;
    QString s;
    QList<RtTask *> list;
    foreach (RtTask *t, taskset->getTasksList())
        list << t;

    qSort(list.begin(), list.end(), prioritySort);

    s += "Interference test:\n";
    latex += "Interference test:\n\n";
    latex += "\\begin{eqnarray*}\n";

    foreach(RtTask *task, list) {
        QList<RtTask *> higherPriorityTasks;
        foreach(RtTask *t, list)
            if ((t->deadline <= task->deadline) && (t != task))
                higherPriorityTasks << t;

        int Tot = 0;
        int prevTot = task->deadline;
        s += QString("%1 = %3").arg(task->name).arg(task->capacity);
        latex += QString("R_{%1} &=& %3").arg(task->name).arg(task->capacity);
        foreach (RtTask *t, higherPriorityTasks) {
            s +=  QString(" + ⌈%1/%2⌉*%3").arg(prevTot).arg(t->period).arg(t->capacity);
            latex +=  QString(R"( + \left \lceil{ \frac{%1}{%2}}\right\rceil \times %3)").arg(prevTot).arg(t->period).arg(t->capacity);
        }

        s += "\n = ";
        latex += "\\\\\n &=& ";
        s += QString(" %3").arg(task->capacity);
        latex += QString(" %3").arg(task->capacity);
        Tot = task->capacity;
        foreach (RtTask *t, higherPriorityTasks) {
            int toAdd = static_cast<int>(ceil(static_cast<double>(prevTot)/static_cast<double>(t->period))*t->capacity);
            s +=  QString(" + %1").arg(toAdd);
            latex +=  QString(" + %1").arg(toAdd);
            Tot += toAdd;
        }

        s += QString("\n = %1\n").arg(Tot);
        latex += QString("\\\\\nR_{%1} &=& \\fbox{%2}\\\\\n").arg(task->name).arg(Tot);

        if (Tot > task->deadline) {
            ok = false;
        }

    }

    latex += "\\end{eqnarray*}\n";

    return s;
}

QString criticalZone(const RtTaskSet *taskset, QString &latex)
{
    QString s;
    QList<RtTask *> list;
    foreach (RtTask *t, taskset->getTasksList())
        list << t;

    qSort(list.begin(), list.end(), prioritySort);

    latex += "\\begin{eqnarray*}\n";

    foreach(RtTask *task, list) {
        QList<RtTask *> higherPriorityTasks;
        foreach(RtTask *t, list)
            if ((t->deadline <= task->deadline) && (t != task))
                higherPriorityTasks << t;

        bool cont = !higherPriorityTasks.isEmpty();


        s += task->name + "_0 = ";
        latex += QString("R_{%1}^0 &=& ").arg(task->name);
        int Tot = task->capacity;
        s += QString("%1").arg(task->capacity);
        if (cont) {
            latex += QString("%1").arg(task->capacity);
        }
        else {
            latex += QString("\\fbox{%1}").arg(task->capacity);
        }
        foreach (RtTask *t, higherPriorityTasks) {
            s +=  QString(" + %1").arg(t->capacity);
            latex +=  QString(" + %1").arg(t->capacity);
            Tot = Tot + t->capacity;
        }

        if (Tot != task->capacity) {
            s += QString (" = %1").arg(Tot);
            latex += QString (" = %1").arg(Tot);
        }
        s += "\n";
        latex += "\\\\\n";

        int iteration = 1;
        while (cont) {
            int prevTot = Tot;
            s += QString("%1_%2 = %3").arg(task->name).arg(iteration).arg(task->capacity);
            latex += QString("R_{%1}^%2 &=& %3").arg(task->name).arg(iteration).arg(task->capacity);
            foreach (RtTask *t, higherPriorityTasks) {
                s +=  QString(" + ⌈%1/%2⌉*%3").arg(prevTot).arg(t->period).arg(t->capacity);
                latex +=  QString(R"( + \left \lceil{ \frac{%1}{%2}}\right\rceil \times %3)").arg(prevTot).arg(t->period).arg(t->capacity);
            }

            s += "\n = ";
            latex += "\\\\\n &=& ";
            s += QString(" %3").arg(task->capacity);
            latex += QString(" %3").arg(task->capacity);
            Tot = task->capacity;
            foreach (RtTask *t, higherPriorityTasks) {
                int toAdd = static_cast<int>(ceil(static_cast<double>(prevTot)/static_cast<double>(t->period))*t->capacity);
                s +=  QString(" + %1").arg(toAdd);
                latex +=  QString(" + %1").arg(toAdd);
                Tot += toAdd;
            }

            s += QString("\n = %1\n").arg(Tot);
            latex += QString("\\\\\n &=& %1\\\\\n").arg(Tot);

            iteration ++;
            if (prevTot == Tot) {
                cont = false;
                s += QString("\n = %1\n").arg(Tot);
                latex += QString("R_{%1} &=& \\fbox{%2}\\\\\n").arg(task->name).arg(Tot);
            }
            else if (Tot > task->deadline) {
                cont = false;
                s += QString("\n >= %1\n").arg(Tot);
                latex += QString("R_{%1} &\\geq& \\fbox{%2}\\\\\n").arg(task->name).arg(Tot);
            }

        }
    }

    latex += "\\end{eqnarray*}\n";

    return s;
}


bool RMSchedulingAnalyzer::schedulable(const RtTaskSet */*taskset*/)
{

    return false;
}

int pgcd(int x,int y)
{
    int a=x;
    int b=y;
    while(b!=0){
        int c=a%b;
        a=b;
        b=c;
    }
    return a;
}

double ulub(int nbTasks)
{
    double nt = nbTasks;
    return nt * (pow(2.0, (1.0 / nt)) - 1.0);
}

QString RMSchedulingAnalyzer::text(const RtTaskSet *taskset, QString &latex)
{
    double charge = 0;
    double product = 1.0;
    int nbTasks = 0;
    foreach (RtTask *task, taskset->getTasksList()) {
        if (task->isPeriodic()) {
            nbTasks ++;
            charge += (static_cast<double>(task->capacity)) / (static_cast<double>(task->period));
            product *= ((static_cast<double>(task->capacity)) / (static_cast<double>(task->period)) + 1.0);
        }
    }


    int denom = 1;
    foreach (RtTask *task, taskset->getTasksList()) {
        if (task->isPeriodic())
            denom = ppcm(denom, task->period);
    }
    int num = 0;
    foreach (RtTask *task, taskset->getTasksList()) {
        if (task->isPeriodic())
        num += task->capacity * denom / task->period;
    }

    int pg = pgcd(num,denom);
    num /= pg;
    denom /= pg;

    QString s;
    s += QString("Charge = (%1/%2) = %3").arg(num).arg(denom).arg(charge);

    if (charge < std::log(2)) {
        s += QString("\n\n");
        s += QString(" La charge est plus petite que U_{lub} = 0.69");
    }
    else if (charge <= ulub(nbTasks)) {
        s += QString("\n\n");
        s += QString(" La charge est plus petite que U_{lub}(%1) = %2. Version Liu et Layland").arg(nbTasks).arg(ulub(nbTasks));
    }
    else if (product <= 2.0) {
            s += QString("\n\n");
            s += QString(" Le critère hyperbolic bound est respecté avec le produit = %1").arg(product);
    } else {
        bool ok;
        s = interference(taskset, latex, ok);
        if (ok) {
            return s;
        }
        return criticalZone(taskset, latex);
    }

    return s;
}

bool DMSchedulingAnalyzer::schedulable(const RtTaskSet */*taskset*/)
{
    return true;
}

QString DMSchedulingAnalyzer::text(const RtTaskSet *taskset, QString &latex)
{
    double charge = 0;
    foreach (RtTask *task, taskset->getTasksList()) {
        if (task->isPeriodic())
            charge += static_cast<double>(task->capacity) / static_cast<double>(task->deadline);
    }

    int denom = 1;
    foreach (RtTask *task, taskset->getTasksList()) {
        if (task->isPeriodic())
            denom = ppcm(denom, task->deadline);
    }
    int num = 0;
    foreach (RtTask *task, taskset->getTasksList()) {
        if (task->isPeriodic())
            num += task->capacity * denom / task->deadline;
    }

    int pg = pgcd(num,denom);
    num /= pg;
    denom /= pg;


    bool ok;
    QString s = interference(taskset, latex, ok);
    if (ok) {
        return s;
    }
    return criticalZone(taskset, latex);

//    QString s;
//    s += QString("Charge = (%1/%2) = %3").arg(num).arg(denom).arg(charge);
//    return s;
}


bool EDFSchedulingAnalyzer::schedulable(const RtTaskSet */*taskset*/)
{
    return true;
}

QString EDFSchedulingAnalyzer::text(const RtTaskSet */*taskset*/, QString &/*latex*/)
{
    QString s;
    s = "Not implemented yet";
    return s;
}
