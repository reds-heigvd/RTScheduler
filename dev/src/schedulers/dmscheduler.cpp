/******************************************************************************
 RTScheduler is a GUI software that performs a static scheduling analysis of
 a set of real-time tasks.

 Copyright (C) 2017  HEIG-VD / Yann Thoma

 This file is part of RTScheduler.

 RTScheduler is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Foobar is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with RTScheduler.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "dmscheduler.h"
#include "schedulinganalyzer.h"

DMScheduler::DMScheduler() : RtScheduler(new DMSchedulingAnalyzer())
{
}

bool DMSort(ActiveTask *task1, ActiveTask *task2)
{
    return task1->taskDesc->deadline < task2->taskDesc->deadline;
}

void DMScheduler::assignPriorities()
{
    qSort(activeTasks.begin(), activeTasks.end(), DMSort);
    for(int i=0;i<activeTasks.size(); i++) {
        activeTasks[i]->currentPriority = activeTasks.size() - i;
    }
}
